package com.example.actuatorsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorSampleApplication {

	public static void main(String[] args) {
	    System.out.println("Run spring boot");
		SpringApplication.run(ActuatorSampleApplication.class, args);
	}
}
